import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    Label {
        id: label
        anchors.centerIn: parent
        //: Title text on cover
        //% "Krisinformation"
        text: qsTrId("app-name")
    }
}
