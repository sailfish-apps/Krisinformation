import QtQuick 2.0
import "../utils/api.js" as API

ListModel {
    property string url: "http://api.krisinformation.se/v1/feed?format=json"
    property bool loading: false

    function load() {
        loading = true;
        clear();
        API.request( function(result, response) {
            if ( result ) {
                var items = response.Entries;
                for (var n = 0; n < items.length; n++) {
                    var item = items[n]
                    item.Ymd = new Date(item.Updated).toDateString()
                    append(item)
                }
            } else {
                console.log ("An error occurred: %1".arg(JSON.stringify(response)));
            }
            loading = false;
        }, url);
    }

    Component.onCompleted: load()

}
