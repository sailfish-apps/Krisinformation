import QtQuick 2.0
import Sailfish.Silica 1.0
import "../utils/api.js" as API

Item {
    property string url
    property bool loading: false
    property bool completed: false
    property var data: ({})

    function load() {

        // For some reason the json api includes link to xml
        url = url.replace("format=xml", "format=json")

        loading = true;
        API.request( function(result, response) {
            if ( result ) {
                data = response.Info[0]
                completed = true
            } else {
                console.log ("An error occurred: %1".arg(JSON.stringify(response)));
            }
            loading = false;
        }, url);
    }

   Component.onCompleted: load()
}
