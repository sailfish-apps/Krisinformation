import QtQuick 2.6
import Sailfish.Silica 1.0

AboutPageForm {
    apiAction.onClicked: Qt.openUrlExternally("https://www.krisinformation.se/om-krisinformation/oppen-data")
    issueAction.onClicked: Qt.openUrlExternally("https://gitlab.com/sailfish-apps/Krisinformation/issues/new")
    sourceAction.onClicked: Qt.openUrlExternally("https://gitlab.com/sailfish-apps/Krisinformation/")
}
