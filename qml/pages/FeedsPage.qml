import QtQuick 2.0
import Sailfish.Silica 1.0
import QtQml.Models 2.3
import "../model"
import "../components"

Page {
    id: feedsPage
    allowedOrientations: Orientation.All

    SilicaListView {
        id: feedsListView
        anchors.fill: parent
        VerticalScrollDecorator { flickable: feedsListView }

        PullDownMenu {
            id: navigationMenu

            MenuItem {
                id: actionSettings
                //: Navigate to AboutPage Label
                //% "About"
                text: qsTrId("about")
                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"));
            }
        }

        header: PageHeader {
            //: Header text for FeedsPage
            //% "News and Events"
            title: qsTrId("feeds-page-title")
        }

        section {
            property: "Ymd"
            criteria: ViewSection.FullString
            delegate: SectionHeader {
                text: section
            }
        }

        model: FeedsDelegateModel { id: feedsModel }

        Connections {
            target: feedsModel
            onSelected: {
                console.log("Open:", linkURL)
                pageStack.push(Qt.resolvedUrl("AlertInfoPage.qml"), { url: linkURL });
            }
        }
    }

    BusyIndicator {
        anchors.centerIn: parent
        size: BusyIndicatorSize.Large
        running: feedsModel.loading
    }
}
