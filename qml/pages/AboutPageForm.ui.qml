import QtQuick 2.6
import Sailfish.Silica 1.0

import "../components"

Page {
    id: aboutPage

    property alias apiAction: apiAction
    property alias issueAction: issueAction
    property alias sourceAction: sourceAction

    SilicaFlickable {
        id: aboutPageFlickable
        anchors.fill: parent
        contentHeight: pageColumns.height
        VerticalScrollDecorator { flickable: aboutPageFlickable }

        Column {
            id: pageColumns
            spacing: Theme.paddingMedium

            anchors {
                left: parent.left
                right: parent.right
                margins: Theme.paddingLarge
                verticalCenter: parent.verticalCenter
            }

            PageHeader {
                //: Label for About title
                //% "About Krisinformation"
                title: qsTrId("app-about")
            }

            Image {
                anchors.horizontalCenter: parent.horizontalCenter
                fillMode: Image.PreserveAspectFit
                source: "/usr/share/icons/hicolor/256x256/apps/harbour-krisinformation.png"
            }


            Label {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.WordWrap
                maximumLineCount: 2
                horizontalAlignment: Text.AlignHCenter
                text: "Krisinformation"
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeExtraSmall
                color: Theme.secondaryColor
                //: App version label
                //% "Version: "
                text: qsTrId("app-version") + Qt.application.version
            }

            FullWidthLabel {
                horizontalAlignment: Text.AlignHCenter
                //: App descritpion label
                //% "This is a unofficial client for krisinformation.se"
                text: qsTrId("app-description")
            }

            FullWidthLabel {
                horizontalAlignment: Text.AlignHCenter
                color: Theme.secondaryColor
                font.italic: true
                //: App sub descritpion label
                //% "Krisinformation.se is run by the Swedish Civil Protection and Emergency Agency (MSB). The content is developed by official joint working groups in different areas. All information contained on the site is confirmed by the authorities and other responsible actors."
                text: qsTrId("app-sub-description")
            }

            Item {
                height: Theme.paddingLarge
                width: parent.width
            }

            ButtonLayout {
                width: parent.width

                Button {
                    id: apiAction
                    //: Label for opening api documentation button
                    //% "API"
                    text: qsTrId("api")
                    ButtonLayout.newLine: true
                }

                Button {
                    id: issueAction
                    //: Label for opening report issue button
                    //% "Report an Issue"
                    //~ Context AboutPage
                    text: qsTrId("report-issue")
                    ButtonLayout.newLine: true
                }

                Button {
                    id: sourceAction
                    //: Label for opening source code button
                    //% "Source code"
                    //~ Context AboutPage
                    text: qsTrId("source-code")
                    ButtonLayout.newLine: true
                }
            }
        }
    }
}
