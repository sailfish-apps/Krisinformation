import QtQuick 2.0
import Sailfish.Silica 1.0
import "../model"
import "../components"

Page {
    id: alertInfoPage
    allowedOrientations: Orientation.All
    property alias url: alertInfoModel.url

    SilicaFlickable {
        anchors.fill: parent

        contentHeight: pageColumn.height
        VerticalScrollDecorator { flickable: alertInfoPage }

        Column {
            id: pageColumn
            spacing: Theme.paddingMedium
            width: parent.width
            anchors {
                left: parent.left
                right: parent.right
                margins: Theme.paddingLarge
            }

            PageHeader {
                title: alertInfoModel.data.Area[0].AreaDesc || ""
            }

            FullWidthLabel {
                font.pixelSize: Theme.fontSizeLarge
                text: alertInfoModel.data.Headline || ""
            }

            FullWidthLabel {
                color: Theme.secondaryColor
                text: alertInfoModel.data.Description || ""
            }

            FullWidthLabel {
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeSmall
                text: alertInfoModel.data.SenderName || "https://www.krisinformation.se/"
            }

            Item {
                height: Theme.paddingLarge
                width: parent.width
            }

            ButtonLayout {
                width: parent.width

                Button {
                    id: readMoreAction
                    //: Label for read more button
                    //% "Read more"
                    text: qsTrId("read more")
                    onClicked: Qt.openUrlExternally(alertInfoModel.data.Web)
                }
            }
        }
    }

    AlertInfoModel {
        id: alertInfoModel
    }
}
