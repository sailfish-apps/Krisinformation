function request(callback, url) {
    var xhr = new XMLHttpRequest;
    xhr.open("GET", url, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            var jsn = JSON.parse(xhr.responseText);
            switch (xhr.status) {
            case 200:
                callback(true, jsn);
                break;
            default:
                callback(false, jsn);
                break;
            }
        }
    }
    xhr.send();
}
