import QtQuick 2.0
import Sailfish.Silica 1.0
import "../model"
import "../components"

ListItem {
    width: ListView.view.width
    contentHeight: Theme.itemSizeHuge
    
    Column {
        anchors {
            left: parent.left
            right: parent.right
            margins: Theme.paddingLarge
            verticalCenter: parent.verticalCenter
        }
        
        FeedListLabel {
            text: Title
            color: highlighted ? Theme.highlightColor : Theme.primaryColor
        }
        
        FeedListLabel {
            text: Summary
            color: highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
            font.pixelSize: Theme.fontSizeExtraSmall
        }

        FeedListLabel {
            text: new Date(Updated).toLocaleTimeString(Locale.ShortFormat)
            font.pixelSize: Theme.fontSizeTiny
            color: highlighted ? Theme.primaryColor : Theme.highlightColor
        }
    }
}
