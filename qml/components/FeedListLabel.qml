import QtQuick 2.0
import Sailfish.Silica 1.0

FullWidthLabel {
    maximumLineCount: 2
    truncationMode: TruncationMode.Elide
}
