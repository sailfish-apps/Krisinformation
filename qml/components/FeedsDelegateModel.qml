import QtQuick 2.0
import Sailfish.Silica 1.0
import QtQml.Models 2.3
import "../model"
import "../components"

DelegateModel {
    signal selected(string linkURL)

    property alias loading: feedsModel.loading
    
    property var lessThan: [
        function(left, right) { return left.Updated > right.Updated }
    ]
    
    property int sortOrder: orderSelector.selectedIndex
    
    function insertPosition(lessThan, item) {
        var lower = 0
        var upper = items.count
        while (lower < upper) {
            var middle = Math.floor(lower + (upper - lower) / 2)
            var result = lessThan(item.model, items.get(middle).model);
            if (result) {
                upper = middle
            } else {
                lower = middle + 1
            }
        }
        return lower
    }
    
    function sort(lessThan) {
        while (unsortedItems.count > 0) {
            var item = unsortedItems.get(0)
            var index = insertPosition(lessThan, item)
            
            item.groups = "items"
            items.move(item.itemsIndex, index)
        }
    }
    
    id: feedsDelegateModel
    
    onSortOrderChanged: items.setGroups(0, items.count, "unsorted")
    items.includeByDefault: false
    
    groups: VisualDataGroup {
        id: unsortedItems
        name: "unsorted"
        
        includeByDefault: true
        onChanged: {
            if (feedsDelegateModel.sortOrder === feedsDelegateModel.lessThan.length)
                setGroups(0, count, "items")
            else
                feedsDelegateModel.sort(feedsDelegateModel.lessThan[feedsDelegateModel.sortOrder])
        }
    }
    
    model: FeedsModel { id: feedsModel }
    delegate: FeedsListDelegate { onClicked: feedsDelegateModel.selected(Link.LinkUrl) }
}
