#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <QGuiApplication>
#include <QQuickView>
#include <sailfishapp.h>
#include <app_version.h>

int main(int argc, char *argv[])
{
    QGuiApplication *app = SailfishApp::application(argc, argv);
    app->setApplicationVersion(QStringLiteral(APP_VERSION) + "-" + QStringLiteral(APP_BUILD_NUMBER));
    QQuickView *view = SailfishApp::createView();

    view->setSource(SailfishApp::pathTo("qml/harbour-krisinformation.qml"));
    view->showFullScreen();
    return app->exec();
}
