<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name></name>
    <message id="app-description">
        <source>This is a unofficial client for krisinformation.se</source>
        <extracomment>App descritpion label</extracomment>
        <translation>Det här är en inofficiell klient för krisinformation.se</translation>
    </message>
    <message id="app-about">
        <source>About Krisinformation</source>
        <extracomment>Label for About title</extracomment>
        <translation>Om Krisinformation</translation>
    </message>
    <message id="app-version">
        <source>Version: </source>
        <extracomment>App version label</extracomment>
        <translation>Version: </translation>
    </message>
    <message id="api">
        <source>API</source>
        <extracomment>Label for opening api documentation button</extracomment>
        <translation>API</translation>
    </message>
    <message id="report-issue">
        <source>Report an Issue</source>
        <extracomment>Label for opening report issue button</extracomment>
        <translation>Rapportera ett problem</translation>
        <extra-Context>AboutPage</extra-Context>
    </message>
    <message id="source-code">
        <source>Source code</source>
        <extracomment>Label for opening source code button</extracomment>
        <translation>Källkod</translation>
        <extra-Context>AboutPage</extra-Context>
    </message>
    <message id="app-name">
        <source>Krisinformation</source>
        <extracomment>Title text on cover</extracomment>
        <translation>Krisinformation</translation>
    </message>
    <message id="read more">
        <source>Read more</source>
        <extracomment>Label for read more button</extracomment>
        <translation>Läs Mer</translation>
    </message>
    <message id="about">
        <source>About</source>
        <extracomment>Navigate to AboutPage Label</extracomment>
        <translation>Om</translation>
    </message>
    <message id="feeds-page-title">
        <source>News and Events</source>
        <extracomment>Header text for FeedsPage</extracomment>
        <translation>Aktuellt</translation>
    </message>
    <message id="app-sub-description">
        <source>Krisinformation.se is run by the Swedish Civil Protection and Emergency Agency (MSB). The content is developed by official joint working groups in different areas. All information contained on the site is confirmed by the authorities and other responsible actors.</source>
        <extracomment>App sub descritpion label</extracomment>
        <translation>Krisinformation.se drivs av Myndigheten för samhällsskydd och beredskap (MSB). Innehållet är framtaget genom myndighetsgemensamma arbetsgrupper inom olika områden. All information som finns på sajten är bekräftad information från myndigheter och övriga ansvariga aktörer.</translation>
    </message>
</context>
</TS>
