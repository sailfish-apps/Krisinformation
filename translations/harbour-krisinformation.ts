<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name></name>
    <message id="app-name">
        <source>Krisinformation</source>
        <extracomment>Title text on cover</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="app-about">
        <source>About Krisinformation</source>
        <extracomment>Label for About title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="app-version">
        <source>Version: </source>
        <extracomment>App version label</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="app-description">
        <source>This is a unofficial client for krisinformation.se</source>
        <extracomment>App descritpion label</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="api">
        <source>API</source>
        <extracomment>Label for opening api documentation button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="report-issue">
        <source>Report an Issue</source>
        <extracomment>Label for opening report issue button</extracomment>
        <translation type="unfinished"></translation>
        <extra-Context>AboutPage</extra-Context>
    </message>
    <message id="source-code">
        <source>Source code</source>
        <extracomment>Label for opening source code button</extracomment>
        <translation type="unfinished"></translation>
        <extra-Context>AboutPage</extra-Context>
    </message>
    <message id="read more">
        <source>Read more</source>
        <extracomment>Label for read more button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="about">
        <source>About</source>
        <extracomment>Navigate to AboutPage Label</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="feeds-page-title">
        <source>News and Events</source>
        <extracomment>Header text for FeedsPage</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="app-sub-description">
        <source>Krisinformation.se is run by the Swedish Civil Protection and Emergency Agency (MSB). The content is developed by official joint working groups in different areas. All information contained on the site is confirmed by the authorities and other responsible actors.</source>
        <extracomment>App sub descritpion label</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
