# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-krisinformation

CONFIG += sailfishapp

# Write version file
VERSION_H = \
"$${LITERAL_HASH}ifndef APP_VERSION" \
"$${LITERAL_HASH}   define APP_VERSION \"$$VERSION\"" \
"$${LITERAL_HASH}   define APP_BUILD_NUMBER \"$$BUILD_NUMBER\"" \
"$${LITERAL_HASH}endif"
write_file($$$$OUT_PWD/app_version.h, VERSION_H)

SOURCES += src/harbour-krisinformation.cpp

DISTFILES += qml/harbour-krisinformation.qml \
    qml/cover/CoverPage.qml \
    qml/components/FeedsDelegateModel.qml \
    qml/components/FullWidthLabel.qml \
    qml/model/FeedsModel.qml \
    qml/model/AlertInfoModel.qml \
    qml/pages/FeedsPage.qml \
    qml/pages/AlertInfoPage.qml \
    qml/pages/AboutPage.qml \
    qml/pages/AboutPageForm.ui.qml

OTHER_FILES += \
    rpm/harbour-krisinformation.changes.in \
    rpm/harbour-krisinformation.changes.run.in \
    rpm/harbour-krisinformation.spec \
    rpm/harbour-krisinformation.yaml \
    translations/*.ts \
    qml/utils/api.js \
    harbour-krisinformation.desktop \

TRANSLATIONS += \
    translations/harbour-krisinformation-sv.ts

CONFIG += \
    sailfishapp_i18n \
    sailfishapp_i18n_idbased

SAILFISHAPP_ICONS = 86x86 108x108 128x128 256x256
